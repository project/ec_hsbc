Payment Module  : ec_hsbc
Original Author : Dublin Drupaller
Settings        : administer > eCommerce configuration > receipt types > ec_hsbc

********************************************************************
DESCRIPTION:

This module allows you to accept payments using the HSBC CPI secure eypatments
integration option

Contact Dublin Drupaller via http://www.DublinDrupaller.com for assistance
or for suggestions, ideas or improvements.
********************************************************************


INSTALLATION
------------------
This version of the ec_hsbc.module requires version 6.x-4.x of the Drupal eCommerce API

Pre-installation note: After downloading the module from Drupal.org, it is recommended you
upload the ec_hsbc module files to /sites/all/modules/ecommerce/ec_hsbc

(a) Enable the module as you would any other Drupal module under ADMINISTER -> SITE BUILDING -> MODULES

(b) Once enabled go to the ec_hsbc settings page which can be found by following these links

ADMINISTER > E-COMMERCE CONFIGURATION -> RECEIPT TYPES ->

(c) You must save your configuration settings prior to making the payment option available.


UNINSTALL
-------------

Uninstall routine is included, ADMINISTER -> SITE BUILDING -> MODULES


NOTES
---------
For support/assistance, or if you have any ideas for improvements, please
contact Dublin Drupaller: dub@dublindrupaller.com